﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _03
{
  
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            dataGridView1.RowHeadersWidth = 100;
        }

        private void button1_Click(object sender, EventArgs e)
        {

            int n = Convert.ToInt32(textBox1.Text);
            int m = Convert.ToInt32(textBox2.Text);         
            Random rnd1 = new Random();
            dataGridView1.RowCount = n;
            dataGridView1.ColumnCount = m;
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    dataGridView1.Rows[i].Cells[j].Value = rnd1.Next(-9,9).ToString() + ":" + rnd1.Next(-9, 9).ToString();

                }
            }

            for (int i = 0; i < dataGridView1.Columns.Count; i++)
            {
                dataGridView1.SetColumnHeader(i, i.ToString());
            }
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                dataGridView1.SetRowHeader(i, i.ToString());
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
    public static class DataGridViewExtinctions
    {
        public static void SetColumnHeader(this DataGridView view, int index, string value)
        {
            view.Columns[index].HeaderText = value;
        }

        public static void SetRowHeader(this DataGridView view, int index, string value)
        {
            view.Rows[index].HeaderCell.Value = value;
        }
    }
}
